Planned Graudation: Fall 2023

Graduation Type: Undergrad Senior Research

Have set research topic: No

Percent completed: 0%

For undergrad/MS students:

Are you planning to publish a peer-reviewd paper? Yes

If yes, do you have a target journal or conference? No

For PhD: You need at least 3 papers published or pending publication (waiting for review, accepted waiting for publication)

How many papers have been published or accepted? [0-3+]

How many papers are pending publication (waiting for review decision, minor revision): [0-3+]

How many papers are in the works (research being done, idea is set): [0-3+]

Checklist for all:

- [x] Overleaf account 

- [x] Comfortable enough with Git to put code and changes in a repo here (emailing code is not allowed)

- [ ] On the NSF mailing list

- [x] On the Discord server (optional if really against Discord, email Raphaela to get added otherwise)

